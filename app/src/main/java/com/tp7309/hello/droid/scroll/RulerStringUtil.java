package com.tp7309.hello.droid.scroll;


import java.util.Locale;

/**
 * author : yany
 * e-mail : yanzhikai_yjk@qq.com
 * time   : 2017/12/25
 * desc   : 处理刻度值字符串
 */

public class RulerStringUtil {
    private static float mFactorCache = 0;
    private static float mDividerCache = 1;

    public static String resultValueOf(float input, float factor) {
        return String.format(Locale.US, "%.1f", input * factor);
    }
}
