package com.tp7309.hello.droid.main

import android.view.View
import android.widget.TextView

import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.widget.BaseHolder

/**
 * Created by jerrywangr on 6/13/2019
 */
class MainMenuHolder(view: View) : BaseHolder(view) {
    internal var menuView: TextView = view.findViewById(R.id.main_list_menu)

}