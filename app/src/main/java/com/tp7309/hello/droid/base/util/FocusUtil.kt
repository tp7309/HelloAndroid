package com.tp7309.hello.droid.base.util

import android.animation.ValueAnimator
import android.view.View
import android.view.animation.DecelerateInterpolator

/**
 * @创建人 weishukai
 * @创建时间 16/10/20 下午7:44
 * @类描述 操作获取和失去焦点的动画
 */
object FocusUtil {

    private val duration = 140

    private val startScale = 1.0f

    //private final static float endScale = 1.14f;

    //    public static final float SCALE_RATE = 1.045f;//一
    val SCALE_RATE = 1.0571f

    /**
     * 当焦点发生变化
     *
     * @param view
     * @param gainFocus
     */
    fun onFocusChange(view: View, gainFocus: Boolean) {
        if (gainFocus) {
            onFocusIn(view, SCALE_RATE)
        } else {
            onFocusOut(view, SCALE_RATE)
        }
    }

    /**
     * 当view获得焦点
     *
     * @param view
     */
    fun onFocusIn(view: View, endScale: Float) {

        val animIn = ValueAnimator.ofFloat(startScale, endScale)
        animIn.duration = duration.toLong()
        animIn.interpolator = DecelerateInterpolator()
        animIn.addUpdateListener { animation ->
            val value = animation.animatedValue as Float
            view.scaleX = value
            view.scaleY = value
        }
        animIn.start()

    }

    /**
     * 当view失去焦点
     *
     * @param view
     */
    fun onFocusOut(view: View, endScale: Float) {
        val animOut = ValueAnimator.ofFloat(endScale, startScale)
        animOut.duration = duration.toLong()
        animOut.interpolator = DecelerateInterpolator()
        animOut.addUpdateListener { animation ->
            val value = animation.animatedValue as Float
            view.scaleX = value
            view.scaleY = value
        }

        animOut.start()
    }

}
