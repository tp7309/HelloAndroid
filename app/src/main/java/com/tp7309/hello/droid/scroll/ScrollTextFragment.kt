package com.tp7309.hello.droid.scroll

import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.view.CommonBaseFragment
import kotlinx.android.synthetic.main.activity_scroll_main.*

class ScrollTextFragment : CommonBaseFragment() {
    override fun getLayoutResourceId(): Int {
        return R.layout.activity_scroll_main
    }

    public override fun initViews() {
        btn_start.setOnClickListener { scroll_content.startScroll(-300, -300) }
    }
}
