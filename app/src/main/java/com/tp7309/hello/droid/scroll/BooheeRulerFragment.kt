package com.tp7309.hello.droid.scroll

import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.view.CommonBaseFragment
import kotlinx.android.synthetic.main.activity_boohee_ruler.*

class BooheeRulerFragment : CommonBaseFragment() {
    override fun getLayoutResourceId(): Int {
        return R.layout.activity_boohee_ruler
    }

    override fun initViews() {
        knl_bottom_head.bindRuler(booheeRuler)
    }
}
