package com.tp7309.hello.droid.main

import android.support.v4.app.Fragment
import com.tp7309.hello.droid.aidl.AIDLFragment
import com.tp7309.hello.droid.contentprovider.ContentProviderFragment
import com.tp7309.hello.droid.customview.CircleImageFragment
import com.tp7309.hello.droid.dev.JavaTestFragment
import com.tp7309.hello.droid.dev.TestFragment
import com.tp7309.hello.droid.misports.MISportsFragment
import com.tp7309.hello.droid.processor.AnnotationProcessorFragment
import com.tp7309.hello.droid.scroll.BooheeRulerFragment
import com.tp7309.hello.droid.scroll.DragButtonFragment
import com.tp7309.hello.droid.scroll.ScrollTextFragment
import kotlin.reflect.KClass

/**
 * Created by jerrywangr on 9/12/2019
 */
object MenuConfig {
    //默认进入菜单
    var defaultMenu: MenuItemBean? = null
    val menuList = mutableListOf<MenuItemBean>()

    init {
        menu(TestFragment::class)
        menu(ContentProviderFragment::class)
        menu(AIDLFragment::class)
        menu(JavaTestFragment::class)

        menu(MISportsFragment::class)
        menu(ScrollTextFragment::class)
        menu(DragButtonFragment::class)
        menu(BooheeRulerFragment::class, "滑动尺")

        menu(CircleImageFragment::class)
        menu(AnnotationProcessorFragment::class)

//        defaultMenu = menuList[3]
    }

    fun menu(classz: KClass<out Fragment>) {
        val title = classz.simpleName!!.replace("Fragment", "")
        menu(classz, title)
    }

    fun menu(classz: KClass<out Fragment>, title: String) {
        menuList.add(MenuItemBean(title, classz))
    }
}