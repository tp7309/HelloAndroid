package com.tp7309.hello.droid.base.util

import android.app.Activity

import java.lang.ref.WeakReference
import java.util.Stack

object ActivityStack {

    /**
     * 返回整个栈
     *
     * @return
     */
    val activityStack = Stack<WeakReference<Activity>>()

    /**
     * 返回栈顶第一个acitivy引用，修复之前get(0)导致的问题
     *
     * @return
     */
    val topStack: Activity?
        get() = if (activityStack.size > 0) {
            activityStack.peek().get()
        } else null

    /**
     * 返回栈大小
     *
     * @return
     */
    fun size(): Int {
        return activityStack.size
    }


    /**
     * 当前栈顶项出栈
     */
    fun popCurrent() {
        val activity = topActivity()
        popActivity(activity)
    }


    /**
     * 指定项以上的栈顶所有项出栈
     *
     * @param currentActivity 指定项
     */
    fun popFront(currentActivity: Class<out Activity>) {
        while (true) {
            val activity = topActivity()
            if (activity != null && activity.javaClass != currentActivity) {
                popActivity(activity)
            } else {
                break
            }
        }
    }

    /**
     * 栈中指定的Activity只保留{@retainNum}个。
     *
     * @param activityClass
     * @param retainNum
     */
    fun popRetain(activityClass: Class<out Activity>, retainNum: Int) {
        var foundCount = 0
        for (i in activityStack.indices.reversed()) {
            val wactivity = activityStack.elementAt(i)
            val activity = wactivity.get()
            if (activity != null && activity.javaClass == activityClass) {
                if (++foundCount > 2) {
                    TLog.d("ATVStack", "popRetain:" + activity
                            + "--" + ActivityStack.size())
                    activityStack.removeElementAt(i)
                    activity.finish()
                }
            }
        }
    }

    /**
     * 判断activity在ActivityStack是否存在
     *
     * @param activity
     * @return boolean
     */
    fun isExsit(activity: Activity?): Boolean {
        if (activity != null) {
            val size = activityStack.size
            for (i in 0 until size) {
                if (activity === activityStack[i].get()) {
                    return true
                }
            }
        }
        return false
    }

    fun popCurrent(currentActivity: Activity) {
        val activity: WeakReference<Activity>
        val size = activityStack.size
        for (i in 0 until size) {
            if (currentActivity === activityStack[i].get()) {
                activity = activityStack[i]
                popActivity(activity)
                return
            }
        }
    }

    /**
     * 所有项出栈
     */
    fun popAll() {
        var i = 0
        val size = activityStack.size
        while (i < size) {
            if (activityStack[i] != null) {
                val activity = activityStack[i].get()
                if (activity != null) {
                    TLog.d("ATVStack", "popAll:" +
                            activity.toString() + "--" + ActivityStack.size())
                    activity.finish()
                }
            }
            i++
        }
        activityStack.clear()
    }

    /**
     * 新项入栈
     *
     * @param activity
     */
    fun pushActivity(activity: Activity) {
        for (a in activityStack) {
            TLog.i("stack", "WeakReference--activity:" + if (a.get() != null) a.get().toString() else a.toString())
        }
        TLog.i("ATVStack", "push:$activity")
        activityStack.add(WeakReference(activity))
    }

    /**
     * 指定项出栈
     *
     * @param activity
     */
    fun popActivity(activity: WeakReference<Activity>?) {
        var activityRef = activity
        if (activityRef != null) {
            if (activityRef.get() != null) {
                TLog.d("ATVStack", "pop:" + activityRef.get()
                        .toString() + "--" + ActivityStack.size())
                activityRef.get()!!.finish()
            }
            activityStack.remove(activityRef)
            activityRef = null
        }
    }

    /**
     * 返回栈顶项
     *
     * @return
     */
    fun topActivity(): WeakReference<Activity>? {
        return if (activityStack.size > 0) {
            activityStack.lastElement()
        } else null
    }

    /**
     * 返回栈底项
     *
     * @return
     */
    fun bottomActivity(): WeakReference<Activity>? {
        return if (activityStack.size > 0) {
            activityStack.firstElement()
        } else null
    }
}
