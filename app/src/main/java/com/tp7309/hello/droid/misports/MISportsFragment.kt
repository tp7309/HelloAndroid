package com.tp7309.hello.droid.misports

import android.os.Handler
import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.view.BaseActivity
import com.tp7309.hello.droid.base.view.CommonBaseFragment
import kotlinx.android.synthetic.main.activity_mi_sports.*

//import kotlinx.android.synthetic.main.activity_mi_sports.*

class MISportsFragment : CommonBaseFragment() {
    private var handler: Handler? = null
    internal var connect = false

    override fun getLayoutResourceId(): Int {
        return R.layout.activity_mi_sports
    }

    override fun initViews() {

    }

    override fun bindEvents() {
        val sportsData = SportsData()
        sportsData.step = 2714
        sportsData.distance = 1700f
        sportsData.calories = 34
        sportsData.progress = 75
        mi_sports_loading_view.setSportsData(sportsData)

        handler = Handler()
        connect_button.setOnClickListener {
            handler!!.postDelayed({
                connect = !connect
                mi_sports_loading_view.setConnected(connect)
                connect_button.text = if (connect) "Disconnect" else "Connect"
            }, 500)
        }
    }

    override fun requestData() {

    }
}
