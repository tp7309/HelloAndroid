package com.tp7309.hello.droid.base.util

import android.text.TextUtils
import android.util.Log
import java.io.PrintWriter
import java.io.StringWriter
import java.text.SimpleDateFormat
import java.util.*

/**
 * Log工具，根据build任务决定log级别
 * 只有ERROR级别的log才会写入文件
 * 默认路径为com.pplive.atv.sports.feedback.AppLogManager#cacheLog
 *
 * @author menglu
 */
object TLog {
    private val DEFAULT_TAG = "HI"
    val YMD_HMS_FORMAT = "yyyy-MM-dd HH:mm:ss"

    private val tagLevel: Int
        get() = Log.VERBOSE

    fun v(content: String) {
        val tag = generateTag()
        v(tag, content)
    }

    @JvmOverloads
    fun v(tag: String, content: String, tr: Throwable? = null) {
        if (tagLevel <= Log.VERBOSE) {
            Log.v(generateTag(tag), content, tr)
        }
    }

    fun d(content: String) {
        d(generateTag(), content)
    }

    @JvmOverloads
    fun d(tag: String, content: String, tr: Throwable? = null) {
        if (tagLevel <= Log.DEBUG) {
            Log.d(generateTag(tag), content, tr)
        }
    }

    fun i(content: String) {
        i(generateTag(), content)
    }

    @JvmOverloads
    fun i(tag: String, content: String, tr: Throwable? = null) {
        if (tagLevel <= Log.INFO) {
            Log.i(generateTag(tag), content, tr)
        }
    }

    fun w(content: String) {
        w(null, content)
    }

    @JvmOverloads
    fun w(tag: String?, content: String, tr: Throwable? = null) {
        if (tagLevel <= Log.WARN) {
            Log.w(generateTag(tag), content, tr)
        }
    }

    fun e(content: String) {
        e(generateTag(), content)
    }

    @JvmOverloads
    fun e(tag: String, content: String, tr: Throwable? = null) {
        if (tagLevel <= Log.ERROR) {
            Log.e(generateTag(tag), content, tr)
            writeLog2File(tag, content, tr)
        }
    }

    /**
     * log写入本地文件
     *
     * @param tag
     * @param content
     * @param tr
     */
    fun writeLog2File(tag: String, content: String, tr: Throwable?) {
        val sb = StringBuilder()
        sb.append('[')
                .append(getTime(System.currentTimeMillis()))
                .append(']')
                .append('[')
                .append(Thread.currentThread().name)
                .append(']')
                .append('[')
                .append(tag)
                .append(']')
        if (!TextUtils.isEmpty(content)) {
            sb.append(content)
        }
        if (tr != null) {
            val sw = StringWriter()
            val pw = PrintWriter(sw, true)
            tr.printStackTrace(pw)
            sb.append(sw.buffer.toString())
        }
        val log = sb.toString()
    }

    private fun getTime(timeMs: Long): String {
        val sdf = SimpleDateFormat(YMD_HMS_FORMAT, Locale.CHINA)
        val date = Date(timeMs)

        return sdf.format(date)
    }

    fun generateTag(tag: String?): String {
        return if (TextUtils.isEmpty(tag)) DEFAULT_TAG else "$DEFAULT_TAG:$tag"

    }


    private fun generateTag(): String {
        // 根据log级别来决定是否要获取tag
        if (tagLevel == Log.ERROR) {
            return ""
        }
        val caller = Throwable().stackTrace[2]
        var tag = "%s.%s(L:%d)"
        var callerClazzName = caller.className
        callerClazzName = callerClazzName.substring(callerClazzName.lastIndexOf(".") + 1)
        tag = String.format(tag, callerClazzName, caller.methodName, caller.lineNumber)
        return tag
    }

}
