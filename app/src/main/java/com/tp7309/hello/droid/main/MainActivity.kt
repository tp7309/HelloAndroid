package com.tp7309.hello.droid.main

import android.content.Intent
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ItemDecoration
import android.view.View
import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.listener.OnViewHolderItemClickListener
import com.tp7309.hello.droid.base.util.SizeUtil
import com.tp7309.hello.droid.base.util.TLog
import com.tp7309.hello.droid.base.view.BaseActivity
import com.tp7309.hello.droid.base.view.ContainerActivity
import com.tp7309.hello.droid.base.widget.BaseGridLayoutManager
import com.tp7309.hello.droid.base.widget.BaseRecyclerView
import kotlin.reflect.KClass

class MainActivity : BaseActivity() {
    private lateinit var mRecyclerView: BaseRecyclerView
    private lateinit var mLayoutManager: BaseGridLayoutManager
    private lateinit var mAdapter: MainAdapter

    override fun getLayoutResourceId(): Int {
        return R.layout.activity_entry_main
    }

    override fun initViews() {
        mRecyclerView = findViewById(R.id.recyclerview)
    }

    override fun bindEvents() {
        //去掉动画,否则当notify数据的时候,焦点会丢失
        mRecyclerView.itemAnimator = null
        val spanCount = if (SizeUtil.sysWidth > SizeUtil.sysHeight) 4 else 3
        mLayoutManager = BaseGridLayoutManager(this, spanCount)
        mAdapter = MainAdapter(this, mutableListOf())
        mRecyclerView.run {
            layoutManager = mLayoutManager
            val spacing = SizeUtil.resetInt(10)
            addItemDecoration(object : ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    if (parent.getChildAdapterPosition(view) % spanCount > 0) {
                        outRect.left = spacing
                    }
                    outRect.bottom = spacing
                }
            })
            adapter = mAdapter
        }
        mAdapter.setOnItemClickListener(object : OnViewHolderItemClickListener<MenuItemBean> {
            override fun onItemClick(holder: RecyclerView.ViewHolder, position: Int, item: MenuItemBean) {
                enterMenu(item)
            }
        })

        //进入默认菜单项
        if (MenuConfig.defaultMenu != null) {
            enterMenu(MenuConfig.defaultMenu!!)
        }
    }

    private fun enterMenu(item: MenuItemBean) {
        TLog.d(TAG, "enter ${item.title}")
        if (item.obj is KClass<*>) {
            val classz = item.obj as KClass<*>
            val intent = Intent(mContext, ContainerActivity::class.java)
            intent.putExtra("name", classz.qualifiedName)
            startActivity(intent)
        }
    }

    override fun requestData() {
        mAdapter.run {
            dataList = MenuConfig.menuList
            notifyDataSetChanged()
        }
    }
}
