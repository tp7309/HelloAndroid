package com.tp7309.hello.droid.aidl

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.os.RemoteException
import android.util.Log
import android.view.View
import android.widget.TextView
import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.util.TLog
import com.tp7309.hello.droid.base.view.CommonBaseFragment
import kotlinx.android.synthetic.main.activity_aidl.*

class AIDLFragment : CommonBaseFragment() {
    override fun getLayoutResourceId(): Int {
        return R.layout.activity_aidl
    }

    //由AIDL文件生成的Java类
    private lateinit var mBookManager: IBookManager

    //标志当前与服务端连接状况的布尔值，false为未连接，true为连接中
    private var mBound = false

    //包含Book对象的list
    private var mBooks: List<Book>? = null
    private var mTvBookList: TextView? = null


    override fun initViews() {
        mTvBookList = view?.findViewById<View>(R.id.main_tv_book_list) as TextView
    }

    override fun bindEvents() {
        add_book.setOnClickListener {
            addBook()
        }
    }

    fun addBook() { //如果与服务端的连接处于未连接状态，则尝试连接
        if (!mBound) {
            attemptToBindService()
            TLog.e(TAG, "当前与服务端处于未连接状态，正在尝试重连，请稍后再试")
            return
        }
        if (mBookManager == null) return
        val book = Book()
        book.name = "APP研发录In"
        book.price = 30
        try {
            mBookManager.addBook(book)
            Log.e(TAG, "addBook: $book")
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }


    private val mOnNewBookArrivedListener = object : IOnNewBookArrivedListener.Stub() {
        @Throws(RemoteException::class)
        override fun onNewBookArrived(newBook: Book) {
            TLog.e(TAG, "onNewBookArrived $newBook")
        }
    }

    /**
     * 尝试与服务端建立连接
     */
    private fun attemptToBindService() {
        val intent = Intent(activity, BookManagerService::class.java)
        activity?.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStart() {
        super.onStart()
        activity?.startActivity(Intent(activity, AIDLActivity::class.java))
//        if (!mBound) {
//            attemptToBindService()
//        }
    }

    override fun onStop() {
        super.onStop()
        if (mBound) {
            activity?.unbindService(mServiceConnection)
            mBound = false
        }
    }

    private val mServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            Log.e(TAG, "service connected")
            mBookManager = IBookManager.Stub.asInterface(service)
            mBound = true
            try {
                mBookManager.registerListener(mOnNewBookArrivedListener)
                mBooks = mBookManager.bookList
                Log.e(TAG, mBooks.toString())
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {
            Log.e(TAG, "service disconnected")
            mBound = false
        }
    }
}
