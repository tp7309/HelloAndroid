package com.tp7309.hello.droid.dev;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.tp7309.hello.droid.R;
import com.tp7309.hello.droid.base.view.CommonBaseFragment;

/**
 * Created by jerrywangr on 10/23/2019
 */
public class JavaTestFragment extends CommonBaseFragment {
    private TextView textButton;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_test;
    }

    @Override
    protected void initViews() {
        textButton = getView().findViewById(R.id.button);
    }

    @Override
    protected void bindEvents() {
        textButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com.hk"));
                startActivity(viewIntent);
            }
        });
    }
}
