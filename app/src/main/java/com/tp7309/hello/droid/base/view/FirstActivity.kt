package com.tp7309.hello.droid.base.view

import android.content.Intent
import com.tp7309.hello.droid.R
import kotlinx.android.synthetic.main.fragment_test.*

/**
 * Created by jerrywangr on 10/24/2019
 */
class FirstActivity : BaseActivity() {
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_test
    }

    override fun initViews() {

    }

    override fun bindEvents() {
        button.setOnClickListener {
            println("enter")
//            CommonToast.showToast("测试")
            val intent = Intent("android.intent.action.APP_A_SECOND_ACTIVITY")
            startActivity(intent)
        }
    }

    override fun requestData() {

    }
}
