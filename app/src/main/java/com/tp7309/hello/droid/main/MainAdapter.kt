package com.tp7309.hello.droid.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.util.FocusUtil
import com.tp7309.hello.droid.base.widget.BaseHolder
import com.tp7309.hello.droid.base.widget.BaseRecyclerAdapter

/**
 * Created by jerrywangr on 6/13/2019
 */
class MainAdapter(context: Context, dataList: List<MenuItemBean>) : BaseRecyclerAdapter<MainMenuHolder, MenuItemBean>(context, dataList) {

    override fun onBindViewHolder(holder: BaseHolder, position: Int) {
        val viewHolder = holder as MainMenuHolder
        val bean = dataList[position]
        viewHolder.menuView.text = bean.title
    }

    override fun onCreateViewHolder(container: ViewGroup, i: Int): BaseHolder {
        val holder = MainMenuHolder(
                LayoutInflater.from(mContext).inflate(R.layout.main_menu_item, container, false))
        holder.itemView.setOnClickListener {
            val pos = holder.adapterPosition
            if (mOnItemClickListener != null && pos >= 0) {
                val bean = dataList[pos]
                mOnItemClickListener.onItemClick(holder, holder.adapterPosition, bean)
            }
        }
        holder.itemView.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            holder.itemView.isSelected = hasFocus
            FocusUtil.onFocusChange(v, hasFocus)
        }
        return holder
    }

    companion object {
        private val TAG = "MainAdapter"
    }
}
