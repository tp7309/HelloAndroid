package com.tp7309.hello.droid.scroll

import android.content.Context
import android.support.annotation.ColorInt
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import com.tp7309.hello.droid.R
import kotlinx.android.synthetic.main.layout_kg_number.view.*

class KgNumberLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : RelativeLayout(context, attrs, defStyleAttr), BooheeRuler.RulerCallback {
    //字体大小
    private var mScaleTextSize = 80
    private var mKgTextSize = 40
    //字体颜色
    @ColorInt
    private var mScaleTextColor = resources.getColor(R.color.colorForgiven)
    @ColorInt
    private var mKgTextColor = resources.getColor(R.color.colorForgiven)
    //kg单位文字
    private var mUnitText = "kg"

    private var mRuler: BooheeRuler? = null

    init {
        initAttrs(context, attrs)
        init()
    }

    private fun init() {
        LayoutInflater.from(context).inflate(R.layout.layout_kg_number, this)
        tv_scale.textSize = mScaleTextSize.toFloat()
        tv_scale.setTextColor(mScaleTextColor)

        tv_kg.textSize = mKgTextSize.toFloat()
        tv_kg.setTextColor(mKgTextColor)
        tv_kg.text = mUnitText
    }

    private fun initAttrs(context: Context, attrs: AttributeSet?) {
        val typedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.KgNumberLayout, 0, 0)
        mScaleTextSize = typedArray.getDimensionPixelSize(R.styleable.KgNumberLayout_scaleTextSize, mScaleTextSize)
        mKgTextSize = typedArray.getDimensionPixelSize(R.styleable.KgNumberLayout_kgTextSize, mKgTextSize)
        mScaleTextColor = typedArray.getColor(R.styleable.KgNumberLayout_scaleTextColor, mScaleTextColor)
        mKgTextColor = typedArray.getColor(R.styleable.KgNumberLayout_kgTextColor, mKgTextColor)
        val text = typedArray.getString(R.styleable.KgNumberLayout_kgUnitText)
        if (text != null) {
            mUnitText = text
        }
        typedArray.recycle()
    }

    fun bindRuler(ruler: BooheeRuler) {
        mRuler = ruler
        mRuler!!.setRulerCallback(this)
    }

    override fun onScaleChanging(scale: Float) {
        tv_scale.text = RulerStringUtil.resultValueOf(scale, mRuler!!.factor)
    }
}
