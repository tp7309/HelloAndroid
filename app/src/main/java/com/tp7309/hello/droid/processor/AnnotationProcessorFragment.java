package com.tp7309.hello.droid.processor;

import android.widget.TextView;

import com.tp7309.hello.droid.R;
import com.tp7309.hello.droid.base.view.CommonBaseFragment;
import com.tp7309.hello.droid.compiler.ZyaoAnnotation;
import com.tp7309.hello.droid.compiler.auto.TP7309;

/**
 * Created by jerrywangr on 10/26/2019
 * 此类不能写成kotlin文件形式，不然注解生成器无法处理！
 */
@ZyaoAnnotation(
        name = "TP",
        text = "Hello !!! Welcome "
)
public class AnnotationProcessorFragment extends CommonBaseFragment {
    TextView testButton;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_test;
    }

    @Override
    protected void initViews() {
        if (getView() != null) {
            testButton = getView().findViewById(R.id.button);
        }
        TP7309 zyao = new TP7309();
        testButton.setText(zyao.getMessage());
    }
}
