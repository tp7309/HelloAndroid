package com.tp7309.hello.droid.scroll

import android.graphics.Rect
import android.view.TouchDelegate
import android.widget.Toast
import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.view.CommonBaseFragment
import kotlinx.android.synthetic.main.activity_touch_delegate_test.*

class TouchDelegateFragment : CommonBaseFragment() {
    override fun getLayoutResourceId(): Int {
        return R.layout.activity_touch_delegate_test
    }

    override fun initViews() {
        touch_delegate_container.post {
            // The bounds for the delegate view (an ImageButton
            // in this example)
            val delegateArea = Rect()
            scroll_content.isEnabled = true
            scroll_content.setOnClickListener {
                Toast.makeText(activity,
                        "Touch occurred within ImageButton touch region.",
                        Toast.LENGTH_SHORT).show()
            }

            // The hit rectangle for the ImageButton
            scroll_content.getHitRect(delegateArea)

            // Extend the touch area of the ImageButton beyond its bounds
            // on the right and bottom.
            delegateArea.right += 100
            delegateArea.bottom += 100

            // Instantiate a TouchDelegate.
            // "delegateArea" is the bounds in local coordinates of
            // the containing view to be mapped to the delegate view.
            // "myButton" is the child view that should receive motion
            // events.
            val touchDelegate = TouchDelegate(delegateArea,
                    scroll_content)

            // Sets the TouchDelegate on the parent view, such that touches
            // within the touch delegate bounds are routed to the child.
            touch_delegate_container.touchDelegate = touchDelegate
        }
    }
}
