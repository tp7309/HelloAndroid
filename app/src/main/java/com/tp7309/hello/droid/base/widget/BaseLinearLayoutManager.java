package com.tp7309.hello.droid.base.widget;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * 能够进行焦点定位的LinearLayoutManager
 */
public class BaseLinearLayoutManager extends LinearLayoutManager {
    private boolean mIsNeedFocus = true;

    public BaseLinearLayoutManager(Context context) {
        super(context);
    }

    public BaseLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public BaseLinearLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * 滚动到的位置是否需要获取焦点,解决TabView在ViewPage滚动自动获取焦点
     */
    public void setNeedFocus(Boolean isNeedFocus) {
        mIsNeedFocus = isNeedFocus;
    }


    /**
     * Base class which scrolls to selected view in onStop().
     */
    abstract class TvLinearSmoothScroller extends LinearSmoothScroller {


        public TvLinearSmoothScroller(Context context) {
            super(context);
        }

        /**
         * 滑动完成后,让该targetPosition 处的item获取焦点
         */
        @Override
        protected void onStop() {
            super.onStop();
            View targetView = findViewByPosition(getTargetPosition());
            if (targetView != null && mIsNeedFocus) {
                targetView.requestFocus();
            }
            mIsNeedFocus =true;
            super.onStop();
        }

    }

    /**
     * RecyclerView的smoothScrollToPosition方法最终会执行smoothScrollToPosition
     *
     * @param recyclerView
     * @param state
     * @param position
     */
    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state,
                                       int position) {
        TvLinearSmoothScroller linearSmoothScroller =
                new TvLinearSmoothScroller(recyclerView.getContext()) {
                    @Override
                    public PointF computeScrollVectorForPosition(int targetPosition) {
                        return computeVectorForPosition(targetPosition);
                    }
                };
        linearSmoothScroller.setTargetPosition(position);
        startSmoothScroll(linearSmoothScroller);
    }


    public PointF computeVectorForPosition(int targetPosition) {
        return super.computeScrollVectorForPosition(targetPosition);
    }


    /**
     * 目的是为了焦点居中
     *
     * @param parent
     * @param child
     * @param rect
     * @param immediate
     * @param focusedChildVisible
     * @return
     */
    @Override
    public boolean requestChildRectangleOnScreen(final RecyclerView parent, View child, Rect rect, boolean immediate, boolean focusedChildVisible) {

        if (parent instanceof BaseRecyclerView) {
            return parent.requestChildRectangleOnScreen(child, rect, immediate);
        } else {
            return super.requestChildRectangleOnScreen(parent, child, rect, immediate, focusedChildVisible);
        }
    }


}
