package com.tp7309.hello.droid.dev;

import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Consumer;

/**
 * Created by jerrywangr on 10/14/2019
 */
public class JTest {

    public static void main(String[] args) {
        Consumer<String> consumer = s -> System.out.println(s);
        consumer.accept("cc");
    }
}
