package com.tp7309.hello.droid.thread

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by tp7309 on 11/15/2019
 */
internal class SyncThread : Runnable {
    override fun run() {
        val threadName = Thread.currentThread().name
        if (threadName.startsWith("A")) {
            normalAsync()
        } else if (threadName.startsWith("S1")) {
            syncThis()
        } else if (threadName.startsWith("S2")) {
            sync2()
        } else if (threadName.startsWith("S3")) {
            sync3()
        } else if (threadName.startsWith("SS1")) {
            staticSync1()
        } else if (threadName.startsWith("SS2")) {
            staticSync2()
        }
    }

    /**
     * 异步方法
     */
    private fun normalAsync() {
        logEnter("normalAsync")
        try {
            logStart("normalAsync")
            Thread.sleep(2000)
            logEnd("normalAsync")
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    /**
     * 方法中有 synchronized(this|object) {} 同步代码块
     */
    private fun syncThis() {
        logEnter("syncThis")
        synchronized(this) {
            try {
                logStart("syncThis")
                Thread.sleep(2000)
                logEnd("syncThis")
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * synchronized 修饰非静态方法
     */
    @Synchronized
    private fun sync2() {
        logEnter("sync2")
        try {
            logStart("sync2")
            Thread.sleep(2000)
            logEnd("sync2")
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    @Synchronized
    private fun sync3() {
        logEnter("sync3")
        try {
            logStart("sync3")
            Thread.sleep(2000)
            logEnd("sync3")
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    companion object {
        @JvmStatic
        @Synchronized
        fun staticSync1() {
            logEnter("staticSync1")
            try {
                logStart("staticSync1")
                Thread.sleep(2000)
                logEnd("staticSync1")
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }

        @JvmStatic
        @Synchronized
        fun staticSync2() {
            logEnter("staticSync2")
            try {
                logStart("staticSync2")
                Thread.sleep(2000)
                logEnd("staticSync2")
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }

        @SuppressLint("SimpleDateFormat")
        fun logEnter(name: String) {
            println(Thread.currentThread().name + "_${name}_enter_" + SimpleDateFormat("HH:mm:ss").format(Date()))
        }

        @SuppressLint("SimpleDateFormat")
        fun logStart(name: String) {
            println(Thread.currentThread().name + "_${name}_start_" + SimpleDateFormat("HH:mm:ss").format(Date()))
        }

        @SuppressLint("SimpleDateFormat")
        fun logEnd(name: String) {
            println(Thread.currentThread().name + "_${name}_end_" + SimpleDateFormat("HH:mm:ss").format(Date()))
        }


    }
}