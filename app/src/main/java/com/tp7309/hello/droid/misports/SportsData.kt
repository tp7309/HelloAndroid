package com.tp7309.hello.droid.misports

/**
 * 小米手环数据 bean
 *
 * Created by sickworm on 2017/10/16.
 */
class SportsData() {
    /**
     * 进度 0-100
     */
    @JvmField
    var progress: Int = 0
    /**
     * 步数
     */
    @JvmField
    var step: Int = 0
    /**
     * 路程 米
     */
    @JvmField
    var distance: Float = 0.toFloat()
    /**
     * 卡路里 千卡
     */
    @JvmField
    var calories: Int = 0

    init {
        progress = 0
        step = 0
        distance = 0f
        calories = 0
    }

    constructor(sportsData: SportsData?) : this() {
        if (sportsData != null) {
            this.progress = sportsData.progress
            this.step = sportsData.step
            this.distance = sportsData.distance
            this.calories = sportsData.calories
        }
    }
}
