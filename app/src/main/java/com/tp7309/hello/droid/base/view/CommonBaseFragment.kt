package com.tp7309.hello.droid.base.view

import android.content.Context
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

import android.view.View.NO_ID

/**
 * base fragmentClass
 *
 * @author chongyouzhao
 * @date 2018/11/28
 */
abstract class CommonBaseFragment : Fragment() {
    //防止RxJava内存泄漏问题
    private val mDisposable = CompositeDisposable()
    protected var mContext: Context? = null
    protected val TAG = javaClass.simpleName

    protected abstract fun getLayoutResourceId(): Int

    protected abstract fun initViews()

    protected open fun bindEvents() {}

    protected open fun requestData() {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutResourceId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //防止直接使用getContext()总报可能为空
        mContext = context
        initViews()
        bindEvents()
        requestData()
    }

    fun <T : View> fv(@IdRes id: Int): T? {
        return if (id == NO_ID) {
            null
        } else view!!.findViewById(id)

    }

    override fun onDestroy() {
        super.onDestroy()
        mDisposable.dispose()
        mContext = null
    }

    protected fun addDisposable(disposable: Disposable) {
        mDisposable.add(disposable)
    }

    fun dispatchKeyEvent(event: KeyEvent): Boolean {
        return false
    }

    fun hasFocus(): Boolean {
        return view != null && view!!.hasFocus()
    }
}
