package com.tp7309.hello.droid.scroll;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.EdgeEffect;
import android.widget.OverScroller;

public class InnerRuler extends View {
    protected Context mContext;
    protected BooheeRuler mParent;

    protected Paint mSmallScalePaint, mBigScalePaint, mTextPaint, mOutLinePaint;
    //当前刻度值
    protected float mCurrentScale = 0;
    //最大刻度数
    protected int mMaxLength = 0;
    //长度、最小可滑动值、最大可滑动值
    protected int mLength, mMinPosition = 0, mMaxPosition = 0;
    //控制滑动
    protected OverScroller mOverScroller;
    //一格大刻度多少格小刻度
    protected int mCount = 10;
    //提前刻画量
    protected int mDrawOffset;
    //速度获取
    protected VelocityTracker mVelocityTracker;
    //惯性最大最小速度
    protected int mMaximumVelocity, mMinimumVelocity;
    //回调接口
//    protected RulerCallback mRulerCallback;
    //边界效果
    protected EdgeEffect mStartEdgeEffect,mEndEdgeEffect;
    //边缘效应长度
    protected int mEdgeLength;

    public InnerRuler(Context context) {
        this(context, null, 0);
    }

    public InnerRuler(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InnerRuler(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mContext = getContext();
    }

    public void refreshSize() {

    }
}
