package com.tp7309.hello.droid.contentprovider

import android.content.ContentValues
import android.net.Uri
import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.view.CommonBaseFragment

/**
 * Created by jerrywangr on 8/30/2019
 */
class ContentProviderFragment : CommonBaseFragment() {

    override fun getLayoutResourceId(): Int {
        return R.layout.main_content_provider
    }

    override fun initViews() {

    }

    override fun bindEvents() {

    }

    override fun requestData() {
        /**
         * 对user表进行操作
         */

        // 设置URI
        val uri_user = Uri.parse("content://cn.scu.myprovider/user")

        // 插入表中数据
        val values = ContentValues()
        values.put("_id", 3)
        values.put("name", "Iverson")


        // 获取ContentResolver
        val resolver = activity!!.contentResolver
        // 通过ContentResolver 根据URI 向ContentProvider中插入数据
        resolver.insert(uri_user, values)

        // 通过ContentResolver 向ContentProvider中查询数据
        val cursor = resolver.query(uri_user, arrayOf("_id", "name"), null, null, null)
        while (cursor!!.moveToNext()) {
            println("query book:" + cursor.getInt(0) + " " + cursor.getString(1))
            // 将表中数据全部输出
        }
        cursor.close()
        // 关闭游标

        /**
         * 对job表进行操作
         */
        // 和上述类似,只是URI需要更改,从而匹配不同的URI CODE,从而找到不同的数据资源
        val uri_job = Uri.parse("content://cn.scu.myprovider/job")

        // 插入表中数据
        val values2 = ContentValues()
        values2.put("_id", 3)
        values2.put("job", "NBA Player")

        // 获取ContentResolver
        val resolver2 = activity!!.contentResolver
        // 通过ContentResolver 根据URI 向ContentProvider中插入数据
        resolver2.insert(uri_job, values2)

        // 通过ContentResolver 向ContentProvider中查询数据
        val cursor2 = resolver2.query(uri_job, arrayOf("_id", "job"), null, null, null)
        while (cursor2!!.moveToNext()) {
            println("query job:" + cursor2.getInt(0) + " " + cursor2.getString(1))
            // 将表中数据全部输出
        }
        cursor2.close()
        // 关闭游标
    }
}