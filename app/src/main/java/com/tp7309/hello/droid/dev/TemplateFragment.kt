package com.tp7309.hello.droid.dev

import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.view.CommonBaseFragment

/**
 * Created by jerrywangr on 10/26/2019
 */
class TemplateFragment : CommonBaseFragment() {
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_test
    }

    override fun initViews() {

    }
}
