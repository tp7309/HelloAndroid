package com.tp7309.hello.droid.base.util

import android.content.Context
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.widget.TextView
import java.math.BigDecimal

object SizeUtil {
    private val UIDesign_W = 1920f
    private val UIDesign_H = 1080f
    private var displayMetrics: DisplayMetrics? = null
    var mDisplayDensity: Float = 0.toFloat()

    val displayRatio: Float
        get() = displayMetrics!!.widthPixels / (displayMetrics!!.heightPixels + 0.0f)

    private var screenOrientation = ScreenOrientation.Landscape
    var screenWidthScale: Float = 0.toFloat()
    var screenHeightScale: Float = 0.toFloat()
    var sysWidth: Int = 0
    var sysHeight: Int = 0

    fun ratio_8_5(): Boolean {
        return if (Math.abs(sysWidth.toFloat() / sysHeight - 1.6) < 0.02) {
            true
        } else false
    }

    private enum class ScreenOrientation {
        Portrait, Landscape
    }

    public fun init(context: Context) {
        displayMetrics = context.resources.displayMetrics
        mDisplayDensity = displayMetrics!!.density
        initScreenScale(displayMetrics!!)
    }

    // Compute the scaling ratio
    private fun initScreenScale(displayMetrics: DisplayMetrics) {
        sysWidth = displayMetrics.widthPixels
        sysHeight = displayMetrics.heightPixels
        if (displayMetrics.widthPixels > displayMetrics.heightPixels) {
            screenOrientation = ScreenOrientation.Landscape
            screenWidthScale = displayMetrics.widthPixels / UIDesign_W
            screenHeightScale = displayMetrics.heightPixels / UIDesign_H
        } else {
            screenOrientation = ScreenOrientation.Portrait
            screenWidthScale = displayMetrics.widthPixels / UIDesign_H
            screenHeightScale = displayMetrics.heightPixels / UIDesign_W
        }
    }

    fun resetInt(value: Int): Int {
        return (value * screenWidthScale).toInt()
    }

    fun resetIntHeight(value: Int): Int {
        return (value * screenHeightScale).toInt()
    }

    fun resetFloat(value: Float): Float {
        return value * screenWidthScale
    }

    // Scale the given view, its contents, and all of its children
    fun resetViewWithScale(v: View?) {
        if (v == null || screenWidthScale == 1f) {
            return
        }
        resetView(v)
        if (v is ViewGroup) {
            val viewGroup = v as ViewGroup?
            for (i in 0 until viewGroup!!.childCount) {
                resetViewWithScale(viewGroup.getChildAt(i))
            }
        }
    }

    fun convertFloatToInt(sourceNum: Float): Int {
        val bigDecimal = BigDecimal(sourceNum.toDouble())
        return if (sourceNum > 0 && sourceNum <= 1) {
            1
        } else {
            bigDecimal.setScale(0, BigDecimal.ROUND_HALF_UP).toInt()
        }
    }

    // Compute the proper scaled size, return minimal size is 1
    private fun getProperScaledSize(viewSize: Int): Int {
        return if (viewSize * screenWidthScale > 0 && viewSize * screenWidthScale < 0.5) 1 else convertFloatToInt(viewSize * screenWidthScale)
    }

    // Scale view to fit, along with itself, its margins, padding, etc.
    private fun resetView(view: View) {
        //text size scale
        if (view is TextView) {
            val newTextSize = view.textSize * screenWidthScale
            view.setTextSize(TypedValue.COMPLEX_UNIT_PX, newTextSize)
        }

        // Scale the view itself, width and height
        // Retrieve the view's layout information
        val layoutparams = view.layoutParams
        if (layoutparams != null) {
            if (layoutparams.width > 0) {
                layoutparams.width = convertFloatToInt(screenWidthScale * layoutparams.width)
            }
            if (layoutparams.height > 0) {
                layoutparams.height = convertFloatToInt(screenWidthScale * layoutparams.height)
            }

            //        // If this view has margins, scale those too
            if (layoutparams is MarginLayoutParams) {
                layoutparams.leftMargin = getProperScaledSize(layoutparams.leftMargin)
                layoutparams.rightMargin = getProperScaledSize(layoutparams.rightMargin)
                layoutparams.topMargin = getProperScaledSize(layoutparams.topMargin)
                layoutparams.bottomMargin = getProperScaledSize(layoutparams.bottomMargin)
            }

            // Set the layout information back into the view
            view.layoutParams = layoutparams
        }

        /**
         * 有个BUG，根目录下的padding获取不到
         */
        //        // Scale the view's padding
        view.setPadding(getProperScaledSize(view.paddingLeft), getProperScaledSize(view.paddingTop), getProperScaledSize(view
                .paddingRight), getProperScaledSize(view.paddingBottom))
        view.minimumWidth = getProperScaledSize(view.minimumWidth)
        view.minimumHeight = getProperScaledSize(view.minimumHeight)
    }
}