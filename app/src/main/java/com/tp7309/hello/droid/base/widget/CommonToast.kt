package com.tp7309.hello.droid.base.widget

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PixelFormat
import android.os.Handler
import android.os.Message
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.WindowManager.LayoutParams
import android.widget.TextView

import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.App
import com.tp7309.hello.droid.base.util.SizeUtil

import java.lang.ref.WeakReference

/**
 */

@SuppressLint("StaticFieldLeak")
object CommonToast {
    private var mManager: WindowManager? = null
    private val mHandler: MyHandler
    private var mRoot: View? = null
    private var mParams: LayoutParams? = null
    private var mTextView: TextView? = null
    private val MSG_REMOVE_TOAST = 0x1

    fun showToast(msg: String, length: Int = 3000) {
        if (mManager == null) return
        cancelToast()
        mTextView!!.text = msg
        mManager!!.addView(mRoot, mParams)
        mHandler.sendEmptyMessageDelayed(MSG_REMOVE_TOAST, length.toLong())
    }

    fun cancelToast() {
        mHandler.removeCallbacksAndMessages(null)
        removeView()
    }

    init {
        mHandler = MyHandler(this)
        initViews()
    }

    private fun initViews() {
        val context = App.sContext
        mManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        mRoot = LayoutInflater.from(context).inflate(R.layout.common_layout_toast, null)
        SizeUtil.resetViewWithScale(mRoot)
        mTextView = mRoot!!.findViewById(R.id.toast_normal_text)
        mParams = LayoutParams()
        val bottom = SizeUtil.resetInt(100)
        mParams!!.height = LayoutParams.WRAP_CONTENT  //高
        mParams!!.width = LayoutParams.WRAP_CONTENT   //宽
        mParams!!.y = bottom
        mParams!!.format = PixelFormat.TRANSLUCENT
        mParams!!.type = LayoutParams.TYPE_TOAST
        mParams!!.flags = (LayoutParams.FLAG_KEEP_SCREEN_ON
                or LayoutParams.FLAG_NOT_FOCUSABLE
                or LayoutParams.FLAG_NOT_TOUCHABLE)
        mParams!!.gravity = Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM
    }

    private fun removeView() {
        try {
            mManager!!.removeView(mRoot)
        } catch (e: Exception) {
            // 防止没有添加view时，移除报错
        }

    }

    private class MyHandler internal constructor(toast: CommonToast) : Handler() {
        private val reference: WeakReference<CommonToast>

        init {
            this.reference = WeakReference(toast)
        }

        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            if (msg.what != MSG_REMOVE_TOAST) return
            val toast = reference.get()
            if (toast?.mManager == null
                    || toast.mRoot == null
                    || toast.mTextView == null)
                return
            toast.removeView()
        }
    }
}
