package com.tp7309.hello.droid.base.listener

import android.support.v4.view.GestureDetectorCompat
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View

/**
 * Created by jerrywangr on 6/13/2019
 */
class RecyclerItemClickListener(recyclerView: RecyclerView, private val clickListener: OnItemClickListener?) : RecyclerView.SimpleOnItemTouchListener() {
    private val gestureDetector: GestureDetectorCompat //v4 兼容包中

    interface OnItemClickListener {
        /**
         * 点击时回调
         *
         * @param view 点击的View
         * @param position 点击的位置
         */
        fun onItemClick(view: View, position: Int)

        /**
         * 长点击时回调
         *
         * @param view 点击的View
         * @param position 点击的位置
         */
        //        void onItemLongClick(View view, int position);
    }

    init {
        gestureDetector = GestureDetectorCompat(recyclerView.context,
                object : GestureDetector.SimpleOnGestureListener() {
                    override fun onSingleTapUp(e: MotionEvent): Boolean {
                        return true
                    }

                    override fun onLongPress(e: MotionEvent) {
                        //                        View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
                        //                        if (childView != null && clickListener != null) {
                        //                            clickListener.onItemLongClick(childView,
                        //                                    recyclerView.getChildAdapterPosition(childView));
                        //                        }
                    }
                })
    }

    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
        val childView = rv.findChildViewUnder(e.x, e.y)
        if (childView != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
            clickListener.onItemClick(childView, rv.getChildAdapterPosition(childView))
            return true
        }
        return false
    }
}
