package com.tp7309.hello.droid.base.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by liuyu on 17/1/20.
 * 使用RecyclerView时,所有的ViewHolder需要继承该ViewHolder
 */
public abstract class BaseHolder extends RecyclerView.ViewHolder {
    public BaseHolder(View itemView) {
        super(itemView);
    }
}
