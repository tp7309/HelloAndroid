package com.tp7309.hello.droid.scroll

import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.view.CommonBaseFragment

/**
 * Created by jerrywangr on 10/17/2019
 */
class DragButtonFragment : CommonBaseFragment() {
    override fun getLayoutResourceId(): Int {
        return R.layout.activity_drag_test
    }

    override fun initViews() {

    }
}
