package com.tp7309.hello.droid.base.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.tp7309.hello.droid.base.listener.OnViewHolderItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liuyu on 17/2/15.
 * <p>
 * note:防止RecyclerView更新数据的时候焦点丢失
 * <p>
 * (1)adapter执行setHasStableIds(true)方法
 * (2)重写getItemId()方法,让每个view都有各自的id
 * (3)RecyclerView的动画必须去掉
 */
public abstract class BaseRecyclerAdapter<VH extends BaseHolder, T> extends RecyclerView.Adapter<BaseHolder> {

    protected Context mContext;

    private List<T> mDataList;
    protected OnViewHolderItemClickListener<T> mOnItemClickListener;

    public BaseRecyclerAdapter(Context context, List<T> dataList) {
        mContext = context;
        this.mDataList = dataList != null ? dataList : new ArrayList<T>();
        setHasStableIds(true);
    }

    public void setDataList(List<T> dataList) {
        mDataList.clear();
        if (dataList != null) {
            mDataList.addAll(dataList);
        }
    }

    public void addDataList(List<T> dataList) {
        this.mDataList.addAll(dataList);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public List<T> getDataList() {
        return mDataList;
    }

    /**
     * @param hasStableIds 有多个observer的话会报错
     */
    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    public void setOnItemClickListener(OnViewHolderItemClickListener<T> onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }
}
