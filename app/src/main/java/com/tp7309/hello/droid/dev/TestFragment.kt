package com.tp7309.hello.droid.dev

import android.content.Intent
import android.view.View
import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.util.TLog
import com.tp7309.hello.droid.base.view.CommonBaseFragment
import kotlinx.android.synthetic.main.fragment_test.*


/**
 * Created by jerrywangr on 9/11/2019
 */
class TestFragment : CommonBaseFragment() {
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_test
    }

    override fun initViews() {
        activity!!.window.decorView.viewTreeObserver.addOnGlobalFocusChangeListener {
            oldFocus, newFocus ->
            TLog.d(TAG, "oldFocus ： $oldFocus; new Focus : $newFocus")
        }
        button.setOnClickListener {
            println("enter")
        }
    }

    private val broadcast = "com.chj"
    fun send(view: View) {
        val intent = Intent()
        intent.run {
            action = broadcast
            putExtra("name", "suzhu")
        }
        activity?.sendBroadcast(intent)
    }

    override fun bindEvents() {

    }

    override fun requestData() {
    }
}