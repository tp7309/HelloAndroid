package com.tp7309.hello.droid.main

/**
 * Created by jerrywangr on 6/13/2019
 */
//data class MenuItemBean(var title: String, var fragmentClass: Class<*>?, var obj: Any?)
class MenuItemBean {
    var id: Int = 0
    var title: String? = null
    var fragmentClass: Class<*>? = null
    var obj: Any? = null

    constructor(title: String, activityClass: Class<*>) {
        this.title = title
        this.fragmentClass = activityClass
    }

    constructor(title: String, `object`: Any) {
        this.title = title
        this.obj = `object`
    }
}
