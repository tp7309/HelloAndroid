package com.tp7309.hello.droid.base.listener

import android.support.v7.widget.RecyclerView

/**
 * Created by jerrywangr on 2018/11/28
 * RecyclerView用的listener
 */
interface OnViewHolderItemClickListener<T> {
    fun onItemClick(holder: RecyclerView.ViewHolder, position: Int, item: T)
}
