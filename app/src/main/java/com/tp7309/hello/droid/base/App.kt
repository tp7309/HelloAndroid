package com.tp7309.hello.droid.base

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.support.multidex.MultiDex
import com.squareup.leakcanary.LeakCanary
import com.tp7309.hello.droid.base.util.SizeUtil
import com.tp7309.hello.droid.base.util.TLog


/**
 * Created by jerrywangr on 6/13/2019
 */
class App : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(base)
    }

    override fun onCreate() {
        super.onCreate()
//        ScreenUtil.init(this)
        sContext = this
        SizeUtil.init(this)
        setupLeakCanary()
//        registerLifeCycle()
    }

    private fun registerLifeCycle() {
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity?) {
                TLog.d(TAG, "onActivityPaused: ${activity?.javaClass?.simpleName}")
            }

            override fun onActivityResumed(activity: Activity?) {
                TLog.d(TAG, "onActivityResumed: ${activity?.javaClass?.simpleName}")
            }

            override fun onActivityStarted(activity: Activity?) {
                TLog.d(TAG, "onActivityStarted: ${activity?.javaClass?.simpleName}")
            }

            override fun onActivityDestroyed(activity: Activity?) {
                TLog.d(TAG, "onActivityDestroyed: ${activity?.javaClass?.simpleName}")
            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
                TLog.d(TAG, "onActivitySaveInstanceState: ${activity?.javaClass?.simpleName}")
            }

            override fun onActivityStopped(activity: Activity?) {
                TLog.d(TAG, "onActivityStopped: ${activity?.javaClass?.simpleName}")
            }

            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
                TLog.d(TAG, "onActivityCreated: ${activity?.javaClass?.simpleName}")
            }
        })
    }

    protected fun setupLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }
        LeakCanary.install(this)
    }

    companion object {
        lateinit var sContext: App
        val TAG: String = "HelloApp"
    }
}
