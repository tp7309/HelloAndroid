package com.tp7309.hello.droid.thread

/**
 * Created by tp7309 on 11/15/2019
 * 测试多线程同步问题
 */
object SyncTest {
    @JvmStatic
    fun main(args: Array<String>) {
        val syncThread = SyncThread()
        val A_thread1 = Thread(syncThread, "A_thread1")
        val A_thread2 = Thread(syncThread, "A_thread2")
        val B_thread1 = Thread(syncThread, "B_thread1")
        val B_thread2 = Thread(syncThread, "B_thread2")
        val S1_thread = Thread(syncThread, "S1_thread")
        val S2_thread = Thread(syncThread, "S2_thread")
        val SS1_thread = Thread(syncThread, "SS1_thread")
        val SS2_thread = Thread(syncThread, "SS2_thread")

//        A_thread1.start()
//        A_thread2.start()
//        B_thread1.start()
//        B_thread2.start()
//        S1_thread.start()
//        S2_thread.start()
        SS1_thread.start()
        SS2_thread.start()
    }
}