package com.tp7309.hello.droid.customview

import android.graphics.BitmapFactory
import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.view.CommonBaseFragment
import kotlinx.android.synthetic.main.fragment_circle_image.*

/**
 * Created by tp7309 on 11/28/2019
 */
class CircleImageFragment : CommonBaseFragment() {
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_circle_image
    }

    override fun initViews() {
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.bg_step_law)
        iv_circle.setImageDrawable(CircleImageDrawable(bitmap))
    }
}