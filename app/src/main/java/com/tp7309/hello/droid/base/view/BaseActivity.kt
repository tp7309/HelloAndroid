package com.tp7309.hello.droid.base.view

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AlertDialog

import com.tp7309.hello.droid.base.util.ActivityStack

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by jerrywangr on 6/13/2019
 */
abstract class BaseActivity : FragmentActivity() {
    val TAG = javaClass.simpleName
    //防止RxJava内存泄漏问题
    private val mDisposable = CompositeDisposable()
    protected lateinit var mContext: Context

    protected abstract fun getLayoutResourceId(): Int

    protected abstract fun initViews()

    protected abstract fun bindEvents()

    protected abstract fun requestData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResourceId())
        ActivityStack.pushActivity(this)
        mContext = this
        initViews()
        bindEvents()
        requestData()
    }

    protected fun alert(title: String) {
        AlertDialog.Builder(this).setTitle(title).create().show()
    }

    public override fun onDestroy() {
        super.onDestroy()
        mDisposable.dispose()
        ActivityStack.popCurrent(this)
    }

    protected fun addDisposable(disposable: Disposable) {
        mDisposable.add(disposable)
    }
}
