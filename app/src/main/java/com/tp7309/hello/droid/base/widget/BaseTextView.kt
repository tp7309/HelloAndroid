package com.tp7309.hello.droid.base.widget

import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.support.v4.view.GravityCompat
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.tp7309.hello.droid.R
import com.tp7309.hello.droid.base.util.FocusUtil
import com.tp7309.hello.droid.base.util.TLog

/**
 * Created by jerrywangr on 9/11/2019
 */
class BaseTextView : TextView {
    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        setTextColor(Color.WHITE)
        gravity = Gravity.CENTER
        isFocusable = true
        setBackgroundResource(R.drawable.common_button_selector)
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        FocusUtil.onFocusChange(this, focused)
    }
}
