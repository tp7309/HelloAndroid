package com.tp7309.hello.droid.scroll;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.IntDef;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver;
import android.widget.EdgeEffect;
import android.widget.OverScroller;

import com.tp7309.hello.droid.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class BooheeRuler extends View {
    private final String TAG = "ruler";
    private Context mContext;
    //尺子Style定义
    public static final int TOP_HEAD = 1, BOTTOM_HEAD = 2, LEFT_HEAD = 3, RIGHT_HEAD = 4;
    private float mLastX;

    @IntDef({TOP_HEAD, BOTTOM_HEAD, LEFT_HEAD, RIGHT_HEAD})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RulerStyle {
    }

    private @RulerStyle
    int mStyle = TOP_HEAD;
    //内部的尺子
//    private InnerRuler mInnerRuler;
    //最小最大刻度值(以0.1kg为单位)
    private int mMinScale = 464, mMaxScale = 2000;
    //光标宽度、高度
    private int mCursorWidth = 8, mCursorHeight = 70;
    //大小刻度的长度
    private int mSmallScaleLength = 30, mBigScaleLength = 60;
    //大小刻度的粗细
    private int mSmallScaleWidth = 3, mBigScaleWidth = 5;
    //数字字体大小
    private int mTextSize = 28;
    //数字Text距离顶部高度
    private int mTextMarginHead = 120;
    //刻度间隔
    private int mInterval = 18;
    //数字Text颜色
    private
    @ColorInt
    int mTextColor = getResources().getColor(R.color.colorLightBlack);
    //刻度颜色
    private
    @ColorInt
    int mScaleColor = getResources().getColor(R.color.colorGray);
    //初始的当前刻度
    private float mCurrentScale = 0;
    //一格大刻度多少格小刻度
    private int mCount = 10;
    //光标drawable
    private Drawable mCursorDrawable;
    //尺子两端的padding
    private int mPaddingStartAndEnd = 0;
    private int mPaddingLeft = 0, mPaddingTop = 0, mPaddingRight = 0, mPaddingBottom = 0;
    //尺子背景
    private Drawable mRulerBackGround;
    private int mRulerBackGroundColor = getResources().getColor(R.color.colorDirtyWithe);
    //是否启用边缘效应
    private boolean mCanEdgeEffect = true;
    //边缘颜色
    private @ColorInt
    int mEdgeColor = getResources().getColor(R.color.colorForgiven);
    //刻度乘积因子
    private float mFactor = 0.1f;

    protected Paint mSmallScalePaint, mBigScalePaint, mTextPaint, mOutLinePaint;
    //最大刻度数
    protected int mMaxLength = 0;
    //长度、最小可滑动值、最大可滑动值
    protected int mLength, mMinPosition = 0, mMaxPosition = 0;
    //控制滑动
    protected OverScroller mOverScroller;
    //提前刻画量
    protected int mDrawOffset;
    //速度获取
    protected VelocityTracker mVelocityTracker;
    protected int mTouchSlop;
    //惯性最大最小速度
    protected int mMaximumVelocity, mMinimumVelocity;
    //回调接口
//    protected RulerCallback mRulerCallback;
    //边界效果
    protected EdgeEffect mStartEdgeEffect, mEndEdgeEffect;
    //边缘效应长度
    protected int mEdgeLength;
    //回调接口
    protected RulerCallback mRulerCallback;

    public BooheeRuler(Context context) {
        this(context, null);
    }

    public BooheeRuler(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BooheeRuler(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs);
        initRuler();
    }

    public interface RulerCallback {
        //选取刻度变化的时候回调
        void onScaleChanging(float scale);
    }

    private void initRuler() {
        mContext = getContext();
        //API小于18则关闭硬件加速，否则setAntiAlias()方法不生效
        if (Build.VERSION.SDK_INT < 18) {
            setLayerType(LAYER_TYPE_NONE, null);
        }
        adjustPadding();
        initRuleBackground();
        initPaints();

        getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                getViewTreeObserver().removeOnPreDrawListener(this);
                refreshSize();
                return false;
            }
        });
    }

    private void initPaints() {
        mOverScroller = new OverScroller(getContext());
        mTouchSlop = ViewConfiguration.get(mContext).getScaledTouchSlop();
        mMaximumVelocity = ViewConfiguration.get(mContext).getScaledMaximumFlingVelocity();
        mMinimumVelocity = ViewConfiguration.get(mContext).getScaledMinimumFlingVelocity();

        mSmallScalePaint = new Paint();
        mSmallScalePaint.setColor(mScaleColor);
        mSmallScalePaint.setStrokeWidth(mSmallScaleWidth);
        mSmallScalePaint.setStrokeCap(Paint.Cap.ROUND);

        mBigScalePaint = new Paint();
        mBigScalePaint.setColor(mScaleColor);
        mBigScalePaint.setStrokeWidth(mBigScaleWidth);
        mBigScalePaint.setStrokeCap(Paint.Cap.ROUND);

        mOutLinePaint = new Paint();
        mOutLinePaint.setColor(mScaleColor);
        mOutLinePaint.setStrokeWidth(0);

        mTextPaint = new TextPaint();
        mTextPaint.setColor(mTextColor);
        mTextPaint.setAntiAlias(true);
        mTextPaint.setTextSize(mTextSize);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    private void refreshSize() {
        Log.d(TAG, "refreshSize");
        initDrawable();

        mMaxLength = mMaxScale - mMinScale;
        mLength = mMaxLength * mInterval;
        mMinPosition = -getWidth() / 2;
        mMaxPosition = mLength - getWidth() / 2;

        mDrawOffset = mCount * mInterval / 2;
        goToScale(mCurrentScale);
    }

    private void initDrawable() {
        mCursorDrawable.setBounds((getWidth() - mCursorWidth) / 2, getHeight() - mCursorHeight,
                (getWidth() + mCursorWidth) / 2, getHeight());
    }

    private void initRuleBackground() {
        if (mRulerBackGround != null) {
            setBackground(mRulerBackGround);
        } else {
            setBackgroundColor(mRulerBackGroundColor);
        }
    }

    private void adjustPadding() {
        mPaddingLeft = mPaddingRight = mPaddingStartAndEnd;
        mPaddingTop = mPaddingBottom = 0;
    }

    private void initAttrs(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.BooheeRuler, 0, 0);
        mMinScale = typedArray.getInteger(R.styleable.BooheeRuler_minScale, mMinScale);
        mMaxScale = typedArray.getInteger(R.styleable.BooheeRuler_maxScale, mMaxScale);
        mCursorWidth = typedArray.getDimensionPixelSize(R.styleable.BooheeRuler_cursorWidth, mCursorWidth);
        mCursorHeight = typedArray.getDimensionPixelSize(R.styleable.BooheeRuler_cursorHeight, mCursorHeight);
        mSmallScaleWidth = typedArray.getDimensionPixelSize(R.styleable.BooheeRuler_smallScaleWidth, mSmallScaleWidth);
        mSmallScaleLength = typedArray.getDimensionPixelSize(R.styleable.BooheeRuler_smallScaleLength, mSmallScaleLength);
        mBigScaleWidth = typedArray.getDimensionPixelSize(R.styleable.BooheeRuler_bigScaleWidth, mBigScaleWidth);
        mBigScaleLength = typedArray.getDimensionPixelSize(R.styleable.BooheeRuler_bigScaleLength, mBigScaleLength);
        mTextSize = typedArray.getDimensionPixelSize(R.styleable.BooheeRuler_numberTextSize, mTextSize);
        mTextMarginHead = typedArray.getDimensionPixelSize(R.styleable.BooheeRuler_textMarginHead, mTextMarginHead);
        mInterval = typedArray.getDimensionPixelSize(R.styleable.BooheeRuler_scaleInterval, mInterval);
        mTextColor = typedArray.getColor(R.styleable.BooheeRuler_numberTextColor, mTextColor);
        mScaleColor = typedArray.getColor(R.styleable.BooheeRuler_scaleColor, mScaleColor);
        mCurrentScale = typedArray.getFloat(R.styleable.BooheeRuler_currentScale, (mMaxScale + mMinScale) / 2);
        mCount = typedArray.getInt(R.styleable.BooheeRuler_count, mCount);
        mCursorDrawable = typedArray.getDrawable(R.styleable.BooheeRuler_cursorDrawable);
        if (mCursorDrawable == null) {
            mCursorDrawable = getResources().getDrawable(R.drawable.cursor_shape);
        }
        mPaddingStartAndEnd = typedArray.getDimensionPixelSize(R.styleable.BooheeRuler_paddingStartAndEnd, mPaddingStartAndEnd);
        mStyle = typedArray.getInt(R.styleable.BooheeRuler_rulerStyle, mStyle);
        mRulerBackGround = typedArray.getDrawable(R.styleable.BooheeRuler_rulerBackGround);
        if (mRulerBackGround == null) {
            mRulerBackGroundColor = typedArray.getColor(R.styleable.BooheeRuler_rulerBackGround, mRulerBackGroundColor);
        }
        mCanEdgeEffect = typedArray.getBoolean(R.styleable.BooheeRuler_canEdgeEffect, mCanEdgeEffect);
        mEdgeColor = typedArray.getColor(R.styleable.BooheeRuler_edgeColor, mEdgeColor);
        mFactor = typedArray.getFloat(R.styleable.BooheeRuler_factor, mFactor);
        typedArray.recycle();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        if ((scrollX | scrollY) == 0) {
            mCursorDrawable.draw(canvas);
        } else {
            canvas.translate(scrollX, scrollY);
            mCursorDrawable.draw(canvas);
            canvas.translate(-scrollX, -scrollY);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawScale(canvas);
    }

    private void drawScale(Canvas canvas) {
        int start = (getScrollX() - mDrawOffset) / mInterval + mMinScale;
        int end = (getScrollX() + getWidth() + mDrawOffset) / mInterval + mMinScale;
        int height = canvas.getHeight();
        for (int i = start; i <= end; i++) {
            int locationX = (i - getMinScale()) * mInterval;
            if (i >= mMinScale && i <= mMaxScale) {
                if (i % mCount == 0) {
                    //画大刻度
                    canvas.drawLine(locationX, height - mBigScaleLength,
                            locationX, height, mBigScalePaint);
                    canvas.drawText(RulerStringUtil.resultValueOf(i, mFactor), locationX,
                            height - mTextMarginHead, mTextPaint);
                } else {
                    canvas.drawLine(locationX, height - mSmallScaleLength,
                            locationX, height, mSmallScalePaint);
                }
            }
        }

        //画轮廓线
        canvas.drawLine(getScrollX(), getHeight(), getScrollX() + getWidth(), getHeight(), mOutLinePaint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        refreshSize();
    }

    @Override
    public void computeScroll() {
        if (mOverScroller.computeScrollOffset()) {
            scrollTo(mOverScroller.getCurrX(), mOverScroller.getCurrY());

            //这是最后OverScroller的最后一次滑动，如果这次滑动完了mCurrentScale不是整数，则把尺子移动到最近的整数位置
            if (!mOverScroller.computeScrollOffset() && mCurrentScale != Math.round(mCurrentScale)) {
                scrollBackToCurrentScale();
            }
            invalidate();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        acquireVelocityTracker(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (!mOverScroller.isFinished()) {
                    mOverScroller.abortAnimation();
                }
                mLastX = event.getX();
                break;
            case MotionEvent.ACTION_MOVE:
                int dx = (int) (event.getX() - mLastX);
                mLastX = event.getX();
                scrollBy(-dx, 0);
                break;
            case MotionEvent.ACTION_UP:
                mVelocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
                int velocityX = (int) mVelocityTracker.getXVelocity();
                if (Math.abs(velocityX) > mMinimumVelocity) {
                    fling(-velocityX);
                    invalidate();
                } else {
                    scrollBackToCurrentScale();
                }
                releaseVelocityTracker();
                break;
            case MotionEvent.ACTION_CANCEL:
                if (!mOverScroller.isFinished()) {
                    mOverScroller.abortAnimation();
                }
                releaseVelocityTracker();
                break;
        }
        return true;
    }

    @Override
    public void scrollTo(int x, int y) {
        Log.d(TAG, "currentX: " + x + ", minPosition: " + mMinPosition);
        x = x > mMinPosition ? x : mMinPosition;
        x = x < mMaxPosition ? x : mMaxPosition;
        if (x != getScrollX()) {
            super.scrollTo(x, y);
        }
        mCurrentScale = scrollXToScale(x);
        if (mRulerCallback != null) {
            mRulerCallback.onScaleChanging(mCurrentScale);
        }
    }

    private float scrollXToScale(int scrollX) {
        return ((float)(scrollX - mMinPosition) / mLength) * mMaxLength + mMinScale;
    }

    private int scaleToScrollX(float scale) {
        return (int) ((scale - mMinScale) / mMaxLength * mLength + mMinPosition);
    }

    private void scrollBackToCurrentScale() {
        int dx = scaleToScrollX(mCurrentScale) - getScrollX();
        if (dx != 0) {
            mOverScroller.startScroll(getScrollX(), 0, dx, 0, 500);
            invalidate();
        }
    }

    public void goToScale(float scale) {
        scrollTo(scaleToScrollX(scale), 0);
    }

    private void fling(int velocityX) {
        mOverScroller.fling(getScrollX(), 0, velocityX, 0,
                mMinPosition - mEdgeLength, mMaxPosition + mEdgeLength,
                0, 0);
    }

    private void acquireVelocityTracker(final MotionEvent event) {
        if (null == mVelocityTracker) {
            mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
    }

    private void releaseVelocityTracker() {
        if (null != mVelocityTracker) {
            mVelocityTracker.recycle();
            mVelocityTracker = null;
        }
    }

    public int getEdgeColor() {
        return mEdgeColor;
    }

    //设置能否使用边缘效果
    public void setCanEdgeEffect(boolean canEdgeEffect) {
        this.mCanEdgeEffect = canEdgeEffect;
    }

    public float getFactor() {
        return mFactor;
    }

    public void setFactor(float factor) {
        this.mFactor = factor;
        postInvalidate();
    }

    public boolean canEdgeEffect() {
        return mCanEdgeEffect;
    }

    public float getCurrentScale() {
        return mCurrentScale;
    }

    //设置尺子当前刻度
    public void setCurrentScale(float currentScale) {
        this.mCurrentScale = currentScale;
        goToScale(mCurrentScale);
    }

    public void setRulerCallback(RulerCallback RulerCallback) {
        this.mRulerCallback = RulerCallback;
    }

    public void setMinScale(int minScale) {
        this.mMinScale = minScale;
    }

    public int getMinScale() {
        return mMinScale;
    }

    public void setMaxScale(int maxScale) {
        this.mMaxScale = maxScale;
    }

    public int getMaxScale() {
        return mMaxScale;
    }

    public void setCursorWidth(int cursorWidth) {
        this.mCursorWidth = cursorWidth;
    }

    public int getCursorWidth() {
        return mCursorWidth;
    }

    public void setCursorHeight(int cursorHeight) {
        this.mCursorHeight = cursorHeight;
    }

    public int getCursorHeight() {
        return mCursorHeight;
    }


    public void setBigScaleLength(int bigScaleLength) {
        this.mBigScaleLength = bigScaleLength;
    }

    public int getBigScaleLength() {
        return mBigScaleLength;
    }

    public void setBigScaleWidth(int bigScaleWidth) {
        this.mBigScaleWidth = bigScaleWidth;
    }

    public int getBigScaleWidth() {
        return mBigScaleWidth;
    }

    public void setSmallScaleLength(int smallScaleLength) {
        this.mSmallScaleLength = smallScaleLength;
    }

    public int getSmallScaleLength() {
        return mSmallScaleLength;
    }

    public void setSmallScaleWidth(int smallScaleWidth) {
        this.mSmallScaleWidth = smallScaleWidth;
    }

    public int getSmallScaleWidth() {
        return mSmallScaleWidth;
    }

    public void setTextMarginTop(int textMarginTop) {
        this.mTextMarginHead = textMarginTop;
    }

    public int getTextMarginHead() {
        return mTextMarginHead;
    }

    public void setTextSize(int textSize) {
        this.mTextSize = textSize;
    }

    public int getTextSize() {
        return mTextSize;
    }

    public void setInterval(int interval) {
        this.mInterval = interval;
    }

    public int getInterval() {
        return mInterval;
    }

    public int getTextColor() {
        return mTextColor;
    }

    public int getScaleColor() {
        return mScaleColor;
    }

    public void setCount(int mCount) {
        this.mCount = mCount;
    }

    public int getCount() {
        return mCount;
    }
}
