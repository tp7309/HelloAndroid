package com.tp7309.hello.droid.base.view

import android.os.Bundle
import android.support.v4.app.Fragment
import com.tp7309.hello.droid.R

/**
 * Created by jerrywangr on 8/30/2019
 */
class ContainerActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val fragmentName = intent.getStringExtra("name")
        try {
            val kClass = Class.forName(fragmentName).kotlin
// Get the object OR a new instance if it doesn't exist
            val fragment = (kClass.objectInstance ?: kClass.java.newInstance()) as Fragment
            supportFragmentManager.beginTransaction().replace(R.id.container, fragment).commitAllowingStateLoss()
        } catch (e: Exception) {
            println(e)
        }
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.activity_container
    }

    override fun initViews() {
    }

    override fun bindEvents() {
    }

    override fun requestData() {
    }
}
