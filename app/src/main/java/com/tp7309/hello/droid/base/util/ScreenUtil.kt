package com.tp7309.hello.droid.base.util

import android.app.Activity
import android.app.Application
import android.content.ComponentCallbacks
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Bundle
import android.util.DisplayMetrics

import java.lang.reflect.Field

/**
 * 描述：修改系统默认dpi的工具,来自今日头条的适配方案
 */
object ScreenUtil {
    var densityDpi: Int = 0
    var scaledDensity: Float = 0.toFloat()
    var density: Float = 0.toFloat()
    lateinit var displayMetrics: DisplayMetrics

    private var mAppDensity = 0f
    private var mAppScaleDensity = 0f
    private var mAppDensityDpi = 0
    private val SIGN_DPI = 640f    // 设计图中的宽度,宽度为DP

    fun init(application: Application) {
        val metrics = application.resources.displayMetrics
        mAppDensityDpi = metrics.densityDpi
        mAppDensity = metrics.density
        mAppScaleDensity = metrics.scaledDensity
        displayMetrics = metrics


        densityDpi = metrics.densityDpi
        density = metrics.density
        scaledDensity = metrics.scaledDensity

        //        registerActivityCreated(application);
        //        registerFontChanged(application);
    }

    // 此方法用于跟随用户设置的系统字体大小变化
    private fun registerFontChanged(application: Application) {
        application.registerComponentCallbacks(
                object : ComponentCallbacks {
                    override fun onConfigurationChanged(newConfig: Configuration?) {
                        if (newConfig != null && newConfig.fontScale > 0) {
                            mAppScaleDensity = application.resources.displayMetrics.scaledDensity
                        }
                    }

                    override fun onLowMemory() {

                    }
                }
        )
    }

    //当activity被创建时，使用自定的dpi
    private fun registerActivityCreated(application: Application) {
        application.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle) {
                if (activity == null || activity.resources == null) {
                    return
                }

                var metrics = getMetricsOnMIUI(activity.resources)
                if (metrics == null) {
                    metrics = activity.resources.displayMetrics
                }

                if (metrics == null) return

                val targetDensity = metrics.widthPixels / SIGN_DPI
                val targetScaleDensity = targetDensity * (mAppScaleDensity / mAppDensity)
                val targetDensityDpi = targetScaleDensity * 160

                metrics.density = targetDensity
                metrics.scaledDensity = targetScaleDensity
                metrics.densityDpi = targetDensityDpi.toInt()

                density = metrics.density
                scaledDensity = metrics.scaledDensity
                densityDpi = targetDensityDpi.toInt()
            }

            override fun onActivityStarted(activity: Activity) {

            }

            override fun onActivityResumed(activity: Activity) {

            }

            override fun onActivityPaused(activity: Activity) {

            }

            override fun onActivityStopped(activity: Activity) {

            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {

            }

            override fun onActivityDestroyed(activity: Activity) {

            }
        })
    }


    /**
     * 解决 MIUI 更改框架导致的 MIUI7 + Android5.1.1 上出现的失效问题 (以及极少数基于这部分 MIUI 去掉 ART 然后置入 XPosed 的手机)
     * 来源于: https://github.com/Firedamp/Rudeness/blob/master/rudeness-sdk/src/main/java/com/bulong/rudeness/RudenessScreenHelper.java#L61:5
     *
     * @param resources [Resources]
     * @return [DisplayMetrics], 可能为 `null`
     */
    private fun getMetricsOnMIUI(resources: Resources): DisplayMetrics? {
        if ("MiuiResources" == resources.javaClass.simpleName || "XResources" == resources.javaClass.simpleName) {
            try {
                val field = Resources::class.java.getDeclaredField("mTmpMetrics")
                field.isAccessible = true
                return field.get(resources) as DisplayMetrics
            } catch (e: Exception) {
                return null
            }

        }
        return null
    }
}
