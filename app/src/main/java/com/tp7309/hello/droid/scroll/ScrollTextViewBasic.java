package com.tp7309.hello.droid.scroll;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Scroller;
import android.widget.TextView;

public class ScrollTextViewBasic extends AppCompatTextView {

    private Paint mPaint;
    private Scroller mScroller;

    public ScrollTextViewBasic(Context context) {
        this(context, null, 0);
    }

    public ScrollTextViewBasic(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScrollTextViewBasic(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.YELLOW);
        mScroller = new Scroller(getContext());
    }

    public void startScroll(int dx, int dy) {
        mScroller.startScroll(getScrollX(), getScrollY(), getScrollX() + dx, getScrollY() + dy, 2000);
        invalidate();
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
        if (mScroller.computeScrollOffset()) {
            scrollTo(mScroller.getCurrX(), mScroller.getCurrY());
            invalidate();
        }
    }

//    @Override
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//        canvas.drawCircle(0, 0, 40f, mPaint);
//    }


}
