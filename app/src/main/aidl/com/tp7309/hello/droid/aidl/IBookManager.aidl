// IBookManager.aidl
package com.tp7309.hello.droid.aidl;

// Declare any non-default types here with import statements

import com.tp7309.hello.droid.aidl.Book;
import com.tp7309.hello.droid.aidl.IOnNewBookArrivedListener;

interface IBookManager {
    List<Book> getBookList(); // 返回书籍列表
   void addBook(inout Book book);
    void registerListener(IOnNewBookArrivedListener listener); // 注册接口
    void unregisterListener(IOnNewBookArrivedListener listener); // 注册接口
}
